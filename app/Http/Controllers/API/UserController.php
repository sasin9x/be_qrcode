<?php

namespace App\Http\Controllers\API;

use App\Helper\Converts;
use App\Http\Controllers\Controller;
use App\Models\AccountBalance;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;

class UserController extends Controller
{
    public function login()
    {
        try {
            if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
                $user = Auth::user();
                $data['token'] =  $user->createToken('MyApp')->accessToken;
                return response()->json([
                    'status' => true,
                    'data' => $data
                ], 200);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Thông tin đăng nhập không chính xác!'
                ], 200);
            }
        } catch (Exception $err) {
            return response()->json([
                'status' => false,
                'message' => 'Có lỗi xảy ra, vui lòng thử lại sau!'
            ], 200);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*]).{8,}$/',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => 'Thông tin nhập vào chưa chính xác!'
            ], 200);
        }
        try {
            $user = new User();
            $user->name = $request->name;
            $user->name_unsigned = Converts::convert_vi_to_en($request->name);
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->password =  Hash::make($request->password);
            $user->address = $request->address;
            $user->save();
            AccountBalance::create();
            return response()->json([
                'status' => true,
            ], 200);
        } catch (Exception $err) {
            return response()->json([
                'status' => false,
                'message' => $err
            ], 200);
        }
    }

    public function getAccount()
    {
        try {
            $user = User::find(auth()->id())->load('account_balance');
            return response()->json([
                'status' => true,
                'data' => $user
            ], 200);
        } catch (Exception $err) {
            return response()->json([
                'status' => false,
                'message' => 'Có lỗi xảy ra, vui lòng thử lại sau!'
            ], 200);
        }
    }

    public function getAccountByPhone(Request $request)
    {
        try {
            $user = User::where('phone', $request->phone)->first();
            if ($user) {
                return response()->json([
                    'status' => true,
                    'data' => $user
                ], 200);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Số điện thoại này chưa được đăng ký trên hệ thống!'
                ], 200);
            }
        } catch (Exception $err) {
            return response()->json([
                'status' => false,
                'message' => 'Có lỗi xảy ra, vui lòng thử lại sau!'
            ], 200);
        }
    }

    public function logout()
    {
        try {
            if (Auth::check()) {
                Auth::user()->token()->revoke();
                return response()->json([
                    'status' => true
                ], 200);
            } else {
                return response()->json([
                    'status' => false
                ], 200);
            }
        } catch (Exception $err) {
            return response()->json([
                'status' => false,
                'message' => 'Có lỗi xảy ra, vui lòng thử lại sau!'
            ], 200);
        }
    }
}
