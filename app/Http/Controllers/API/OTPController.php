<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mail\SentMail;
use App\Models\otp;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Mail;

class OTPController extends Controller
{
    public function sentOTP(Request $request)
    {
        try {
            $data = [
                'title' => 'Trần Quang Duy',
                'body' => substr(str_shuffle("0123456789"), 0, 6)
            ];
            $currentOTP = otp::where('userId', auth()->id())->first();
            if ($currentOTP) {
                $currentOTP->otp = $data['body'];
                $currentOTP->save();
            } else {
                $otpData['userId'] = auth()->id();
                $otpData['otp'] = $data['body'];
                otp::create($otpData);
            }
            Mail::to(auth()->user()->email)->send(new SentMail($data));
            return  response()->json([
                'status' => true
            ], 200);
        } catch (Exception $err) {
            return response()->json([
                'status' => false,
                'message' => $err
            ], 200);
        }
    }

    public function checkOTP(Request $request)
    {
        try {
            $currentOTP = otp::where('userId', auth()->id())->first();
            $currentTime = Carbon::now();
            $optTime = new Carbon($currentOTP['updated_at']);
            if ($currentTime->diffInMinutes($optTime) === 0) {
                if ($currentOTP['otp'] == $request->otp) {
                    return  response()->json([
                        'status' => true
                    ], 200);
                }
                return  response()->json([
                    'status' => false,
                    'message' => 'Mã OTP không hợp lệ!'
                ], 200);
            }
            return  response()->json([
                'success' => false,
                'message' => 'Mã OTP không hợp lệ!'
            ], 200);
        } catch (Exception $err) {
            return response()->json([
                'status' => false,
                'message' => $err
            ], 200);
        }
    }
}
