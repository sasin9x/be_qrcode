<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\AccountBalance;
use App\Models\PaymentHistory;
use Exception;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function payment(Request $request)
    {
        try {
            $sender = AccountBalance::find(auth()->id());

            if ($request->money > 0 && $request->money < $sender->money) {
                $sender->money -= $request->money;
                $sender->save();
                $reciver = AccountBalance::find($request->reciverId);
                $reciver->money += $request->money;
                $reciver->save();
                if ($request->reciverId != auth()->id()) {
                    $hitory["senderId"] = auth()->id();
                    $hitory["reciverId"] = $request->reciverId;
                    $hitory["money"] = $request->money;
                    $hitory["content"] = $request->content;
                    PaymentHistory::create($hitory);
                }
                return response()->json([
                    'status' => true
                ], 200);
            } else {
                return response()->json([
                    'status' => false,
                ], 200);
            }
        } catch (Exception $err) {
            return response()->json([
                'status' => false,
                'message' => $err
            ], 200);
        }
    }

    public function recharge(Request $request)
    {
        try {
            $money = AccountBalance::find(auth()->id());
            $money['balance'] += $request->money;
            $money->save();
            return response()->json([
                'status' => true,
            ], 200);
        } catch (Exception $err) {
            return response()->json([
                'status' => false,
                'message' => $err
            ], 200);
        }
    }

    public function getPaymentHistory()
    {
        try {
            $sender  = PaymentHistory::where('senderId', auth()->id())->orderBy('created_at', 'desc')->get()->load('reciver');
            $reciver  = PaymentHistory::where('reciverId', auth()->id())->orderBy('created_at', 'desc')->get()->load('sender');
            return response()->json([
                'sender' => $sender, 
                'reciver' => $reciver
            ], 200);
        } catch (Exception $err) {
            return response()->json([
                'status' => false,
                'message' => $err
            ], 200);
        }
    }
}
