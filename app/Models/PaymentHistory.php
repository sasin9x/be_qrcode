<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentHistory extends Model
{
    use HasFactory;

    protected $table = "payment_histories";

    protected $fillable = [
        'senderId',
        'reciverId',
        'money',
        'content'
    ];

    public function sender()
    {
        return $this->belongsTo('\App\Models\User', 'senderId', 'id');
    }

    public function reciver()
    {
        return $this->belongsTo('\App\Models\User', 'reciverId', 'id');
    }
}
