<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Đăng nhập, Đăng ký
Route::post('login', 'App\Http\Controllers\API\UserController@login');
Route::post('register', 'App\Http\Controllers\API\UserController@register');
// Quên mật khẩu
Route::post('forgot-password', 'App\Http\Controllers\API\OTPController@sentOTP');
// Route::post('check-OTP', 'App\Http\Controllers\API\OTPController@checkOTP');

// Sau khi đăng nhập thành công
Route::group(['middleware' => 'auth:api'], function() {
    // Các thông tin cá nhân
    Route::get('account', 'App\Http\Controllers\API\UserController@getAccount');
    Route::post('account-by-phone', 'App\Http\Controllers\API\UserController@getAccountByPhone');
    Route::get('payment-history', 'App\Http\Controllers\API\PaymentController@getPaymentHistory');
    // Thanh toán điện tử
    Route::post('sent-OTP', 'App\Http\Controllers\API\OTPController@sentOTP');
    Route::post('check-OTP', 'App\Http\Controllers\API\OTPController@checkOTP');
    Route::post('payment', 'App\Http\Controllers\API\PaymentController@payment');
    Route::post('recharge', 'App\Http\Controllers\API\PaymentController@recharge');
    // Đăng xuất
    Route::get('logout', 'App\Http\Controllers\API\UserController@logout');
});