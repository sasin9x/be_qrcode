<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('Tên người dùng');
            $table->string('name_unsigned')->comment('Tên người dùng không dấu');
            $table->string('email')->unique()->comment('Email người dùng');
            $table->string('phone')->unique()->comment('Số điện thoại người dùng');
            $table->string('address')->nullable()->comment('Địa chỉ người dùng');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->comment('Mật khẩu người dùng');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
